#[derive(Serialize, Deserialize)]
pub struct NhentaiResult {
    #[serde(rename = "result")]
    pub galleries: Vec<Gallery>,

    pub num_pages: i64,
    per_page: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Gallery {
    pub id: Id,
    media_id: String,
    pub title: Title,
    pub images: Images,
    scanlator: String,
    upload_date: i64,
    pub tags: Vec<Tag>,
    num_pages: i64,
    num_favorites: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Images {
    #[serde(rename = "pages")]
    pub pages: Vec<Image>,

    #[serde(rename = "cover")]
    cover: Image,

    #[serde(rename = "thumbnail")]
    thumbnail: Image,
}

#[derive(Serialize, Deserialize)]
pub struct Image {
    #[serde(rename = "t")]
    image_type: ImageType,
    #[serde(rename = "w")]
    width: i64,
    #[serde(rename = "h")]
    height: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Tag {
    id: i64,
    #[serde(rename = "type")]
    pub tag_type: TagType,
    pub name: String,
    url: String,
    count: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Title {
    english: String,
    japanese: String,
    pub pretty: String,
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum Id {
    Integer(i64),
    String(String),
}

#[derive(Serialize, Deserialize)]
pub enum ImageType {
    #[serde(rename = "j")]
    Jpg,
    #[serde(rename = "p")]
    Png,
}

#[derive(Serialize, Deserialize, PartialEq)]
pub enum TagType {
    #[serde(rename = "artist")]
    Artist,
    #[serde(rename = "category")]
    Category,
    #[serde(rename = "character")]
    Character,
    #[serde(rename = "group")]
    Group,
    #[serde(rename = "language")]
    Language,
    #[serde(rename = "parody")]
    Parody,
    #[serde(rename = "tag")]
    Tag,
}
