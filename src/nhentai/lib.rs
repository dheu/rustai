#![feature(uniform_paths)]

#[macro_use]
extern crate serde_derive;
extern crate reqwest;
extern crate serde;
extern crate serde_json;

mod api;

use std::cmp::Ordering;
use std::process;
use api::{Gallery, NhentaiResult, TagType};
use std::fmt;

const APIENDPOINT: &str = "https://nhentai.net/api";
const APISEARCH: &str = "/galleries/search?query=";
const APIPAGE: &str = "&page=";
const APIGALLERY: &str = "/gallery/";

impl fmt::Debug for Gallery {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let gav: Vec<&str> = self.tags.iter()
            .filter(|ts| ts.tag_type == TagType::Artist)
            .map(|t| &t.name[..])
            .collect();
        let art = gav.join(" ");

        write!(f, "Title:{}\nArtists:{}", self.title.pretty, art)
    }
}

/// get gallery information
pub fn gallery(id: &str) -> Gallery {
    api_get(&[APIGALLERY, id].concat())
}

/// search for query string on nhentai
pub fn search(q: &str) {
    for g in itpage(q, 1) {
        println!("    {:?}", g);
    }
}

fn itpage(query: &str, i: i64) -> Vec<Gallery> {
    let res: NhentaiResult =
        api_get(&[APISEARCH, query, APIPAGE, &i.to_string()[..]].concat());

    match res.num_pages.cmp(&i) {
        Ordering::Less => {
            println!("no results for query: {}", query);
            return vec![]
        },
        Ordering::Equal => { res.galleries },
        Ordering::Greater => {
            res.galleries.into_iter().chain(itpage(query, i+1)).collect()
        },
    }
}

/// general api endpoint
fn api_get<T: serde::de::DeserializeOwned>(k: &str) -> T {
    match reqwest::get(&[APIENDPOINT, k].concat()) {
        Ok(mut res) => { res.json().unwrap() },
        Err(e) => {
            println!("Couldn't connect to api, {}", e);
            process::exit(1);
        }
    }
}
